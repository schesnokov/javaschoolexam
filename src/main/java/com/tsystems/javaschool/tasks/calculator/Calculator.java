package com.tsystems.javaschool.tasks.calculator;

import java.text.NumberFormat;
import java.util.LinkedList;
import java.util.Locale;
import java.util.NoSuchElementException;

public class Calculator {
    private static final char PLUS_OPERATOR = '+';
    private static final char MINUS_OPERATOR = '-';
    private static final char MULTIPLY_OPERATOR = '*';
    private static final char DIV_OPERATOR = '/';
    private static final char DELIMITER = '.';
    private static final char OPENING_PARENTHESIS = '(';
    private static final char CLOSING_PARENTHESIS = ')';

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        LinkedList<Double> numbers = new LinkedList<>();
        LinkedList<Character> operators = new LinkedList<>();
        try {
            if (statement == null || statement.length() == 0 || statement.contains(",")) return null;
            int k = 1; // It's for comparing 2 neighbour chars at statement.
            for (int i = 0; i < statement.length() - 1; i++) {
                if ((statement.charAt(i) == PLUS_OPERATOR || statement.charAt(i) == MINUS_OPERATOR ||
                        statement.charAt(i) == MULTIPLY_OPERATOR || statement.charAt(i) == DIV_OPERATOR || statement.charAt(i) == DELIMITER) && (statement.charAt(i) == statement.charAt(k)))
                    return null;
                else {
                    k++;
                }
            }
            for (int i = 0; i < statement.length(); i++) {
                char symbol = statement.charAt(i);
                if (symbol == OPENING_PARENTHESIS)
                    operators.add(OPENING_PARENTHESIS);
                else if (symbol == CLOSING_PARENTHESIS) {
                    while (operators.getLast() != OPENING_PARENTHESIS)
                        mathOperation(numbers, operators.removeLast());
                    operators.removeLast();
                } else if (isOperator(symbol)) {
                    while (!operators.isEmpty() && priority(operators.getLast()) >= priority(symbol))
                        mathOperation(numbers, operators.removeLast());
                    operators.add(symbol);
                } else {
                    String operand = "";
                    while ((i < statement.length() && Character.isDigit(statement.charAt(i))) || (i < statement.length() && statement.charAt(i) == DELIMITER))
                        operand += statement.charAt(i++);
                    --i;
                    numbers.add(Double.parseDouble(operand));
                }
            }
            while (!operators.isEmpty())
                mathOperation(numbers, operators.removeLast());
            NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
            numberFormat.setMaximumFractionDigits(4);
            numberFormat.setMinimumFractionDigits(0);
            return numberFormat.format(numbers.get(0));
        } catch (NoSuchElementException | NumberFormatException | ArithmeticException | IndexOutOfBoundsException e) {
            return null;
        }
    }

    private static boolean isOperator(char symbol) {
        return symbol == PLUS_OPERATOR || symbol == MINUS_OPERATOR || symbol == MULTIPLY_OPERATOR || symbol == DIV_OPERATOR;
    }

    private static int priority(char op) {
        switch (op) {
            case PLUS_OPERATOR:
            case MINUS_OPERATOR:
                return 1;
            case MULTIPLY_OPERATOR:
            case DIV_OPERATOR:
                return 2;
            default:
                return -1;
        }
    }

    private static void mathOperation(LinkedList<Double> vars, char op) {
        Double first = vars.removeLast();
        Double second = vars.removeLast();
        switch (op) {
            case PLUS_OPERATOR:
                vars.add(second + first);
                break;
            case MINUS_OPERATOR:
                vars.add(second - first);
                break;
            case MULTIPLY_OPERATOR:
                vars.add(second * first);
                break;
            case DIV_OPERATOR:
                if (first == 0) break;
                vars.add(second / first);
                break;
        }
    }
}
