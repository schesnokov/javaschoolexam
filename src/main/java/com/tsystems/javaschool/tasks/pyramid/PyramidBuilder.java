package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (!isTriangular(inputNumbers) || inputNumbers.contains(null)) throw new CannotBuildPyramidException();
        inputNumbers.sort((a, b) -> Integer.compare(b, a));
        int rows = (int) (Math.sqrt(1 + 8 * inputNumbers.size()) - 1) / 2;
        int columns = 2 * rows - 1;
        int[][] pyramid = new int[rows][columns];
        int j = 0, startPoint = columns, stopPoint = -1;
        for (int i = rows - 1; i >= 0; i--) {
            startPoint--;
            stopPoint++;
            for (int k = startPoint; k >= stopPoint; k = k - 2) {
                pyramid[i][k] = inputNumbers.get(j);
                j++;
            }
        }
        return pyramid;
    }

    private boolean isTriangular(List<Integer> inputNumbers) {
        double n = (Math.sqrt(1 + 8 * inputNumbers.size()) - 1) / 2;
        return (n % 1 == 0);
    }


}
