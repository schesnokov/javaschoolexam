package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        boolean result = false;
        if (x == null || y == null) throw new IllegalArgumentException();
        if (x.size() == 0) return true;
        if (x.size() > y.size()) return false;

        int counter = 0;
        int a = 0; // It's for point in second sequence where the next comparison should be started

        for (int i = 0; i < x.size(); i++) {
            for (int k = a; k < y.size(); k++) {
                if (x.get(i).equals(y.get(k))) {
                    counter++;
                    a = k + 1;
                    if (x.size() == counter) {
                        result = true;
                        return result;
                    }
                    break;
                }
            }
        }
        return result;
    }
}
